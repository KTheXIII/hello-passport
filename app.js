const express = require('express')
const consola = require('consola')
const expressLayouts = require('express-ejs-layouts')
const mongoose = require('mongoose')
const flash = require('connect-flash')
const session = require('express-session')
const passport = require('passport')

const db = require('./configs/keys')

const app = express()

// Passport config
require('./configs/passport')(passport)

// Connect to MongoDB
mongoose
  .connect(db.MongoURI, {
    auth: {
      authSource: 'admin',
    },
    user: db.user,
    pass: db.pass,
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => {
    consola.success('Database connected!')
  })
  .catch((err) => {
    consola.error(err)
  })

// EJS
app.use(expressLayouts)
app.set('view engine', 'ejs')

// Bodyparser
app.use(
  express.urlencoded({
    extended: false,
  })
)

// Express Session
app.set('trust proxy', 1) // trust first proxy
app.use(
  session({
    secret: 'secret',
    resave: true,
    saveUninitialized: true,
  })
)

// Passport middleware
app.use(passport.initialize())
app.use(passport.session())

// Connect flash
app.use(flash())

// Global vars
app.use((req, res, next) => {
  res.locals.success_msg = req.flash('success_msg')
  res.locals.error_msg = req.flash('error_msg')
  res.locals.error = req.flash('error')
  next()
})

app.use('/', require('./routes/index'))
app.use('/users', require('./routes/users'))

const PORT = process.env.PORT | 5000

app.listen(PORT, () => {
  consola.ready({
    message: `Server listening on http://localhost:${PORT}`,
    badge: true,
  })
})
