# Hello Passport!

Learning passportjs, session, ejs.

## Requirements

### Development
  - [nodejs](https://nodejs.org/en/)
  - [docker](https://www.docker.com)

### Production
  - [docker](https://docs.docker.com/engine/install/)
  - [docker-compose](https://docs.docker.com/compose/install/)

## Run application

### Development

In development the `MongoURI` in [keys.js](configs/keys.js), `mdb` needs to be change to `localhost`. I haven't set up process.env so it needs to be change manually.

### Run MongoDB container
```shell
docker-compose -f docker-compose.dev.yml
```

#### Install the packages

Using `npm`
```shell
npm install
```

Using `yarn`
```shell
yarn
```

#### Run server in dev

```shell
npm/yarn run dev
```

### Production
For production just run the command below and docker will spin up 2 containers that is `mongodb` and the `webapp`.

```shell
docker-compose up -d
```

To stop the containers just run
```shell
docker-compose down
```